//
//  PostViewController.swift
//  ParseStarterProject-Swift
//
//  Created by Richmond Ko on 20/09/2016.
//  Copyright © 2016 Parse. All rights reserved.
//

import UIKit
import Parse

class PostViewController: UIViewController, UINavigationControllerDelegate, UIImagePickerControllerDelegate {

    @IBOutlet var imageToPost: UIImageView!
    @IBOutlet var messageTextField: UITextField!
    var activityIndicator = UIActivityIndicatorView()
    
    func showLoading() {
        activityIndicator = UIActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 100, height: 100))
        activityIndicator.center = self.view.center
        activityIndicator.hidesWhenStopped = true
        activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.gray
        view.addSubview(activityIndicator)
        activityIndicator.startAnimating()
        UIApplication.shared.beginIgnoringInteractionEvents()
    }
    
    func stopLoading() {
        self.activityIndicator.stopAnimating()
        UIApplication.shared.endIgnoringInteractionEvents()
    }
    
    func createAlert(title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
            alert.dismiss(animated: true, completion: nil)
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func chooseAnImage(_ sender: AnyObject) {
        
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
        imagePicker.allowsEditing = false
        
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
            
            imageToPost.image = image
        }
        
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func postImage(_ sender: AnyObject) {
        let imageData1 = UIImageJPEGRepresentation(imageToPost.image!, 0.5)
        let imageData2 = UIImageJPEGRepresentation(UIImage(named: "person-icon-5.png")!, 0.5)
        
        if messageTextField.text == "" { //check if there is message
            self.createAlert(title: "Message Missing", message: "Please enter a message")
        } else if imageData1 == imageData2 {
            self.createAlert(title: "Image Missing", message: "Please choose an image to post")
        } else {
            showLoading()
            let post = PFObject(className: "Posts")
            post["message"] = messageTextField.text
            post["userid"] = PFUser.current()?.objectId!
            let imageFile = PFFile(name: "image.png", data: imageData1!)
            post["imageFile"] = imageFile
            
            post.saveInBackground { (success, error) in
                self.stopLoading()
                if error != nil {
                    self.createAlert(title: "Could not post image", message: "Please try again later")
                } else {
                    self.createAlert(title: "Image Posted", message: "Your image has been posted successfully")
                    self.messageTextField.text = ""
                    self.imageToPost.image = UIImage(named: "person-icon-5.png")
                }
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
